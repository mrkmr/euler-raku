my $primes = <2>.SetHash;

#sub fill_primes_leq($num) {
#    say @primes.tail;
#    if @primes.tail = 2
#}

# Check if divisible by all primes less than sqrt($num).
sub is_prime ($num) {
    if $primes<$num> {
        return True;
    }

    # Return False early if we are divisble by any.
    for 3..floor(sqrt($num)) {
        if is_prime($_) {
            if $num % $_ == 0 {
                return False;
            }
        }
    }

    $primes.set($num);
    return True;
}

say is_prime(16);
say is_prime(13195);

#for 1..6 {
#    if is_prime($_) {
#        say "$_";
#    }
#}
