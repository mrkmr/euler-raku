my $one = 1;
my $two = 2;

say "1";

my $sum = 0;
while $two <= 4000000 {
    if $two % 2 == 0 {
        say $two;
        $sum += $two;
    }

    # Do fibonacci update.
    my $tmpone = $one;
    $one = $two;
    $two = $tmpone + $two;
}

say "sum: $sum";
